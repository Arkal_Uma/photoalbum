# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
Prerequisites

The setups steps expect following tools installed on the system.

    
    Rails 5.0.6
    mysql2 0.3.18 < 0.5

1. Create and setup the database

Run the following commands to create and setup the database.

bundle exec rake db:create
bundle exec rake db:migrate

2. Start the Rails server

You can start the rails server using the command given below.

bundle exec rails s
