class AddTaglinePhotoToPhotos < ActiveRecord::Migration[5.0]
  def change
    add_column :photos, :tagline_photo, :string
  end
end
