class RemoveOwnerFromPhotos < ActiveRecord::Migration[5.0]
  def change
    remove_column :photos, :owner, :string
  end
end
