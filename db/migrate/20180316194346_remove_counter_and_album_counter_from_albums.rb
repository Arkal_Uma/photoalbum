class RemoveCounterAndAlbumCounterFromAlbums < ActiveRecord::Migration[5.0]
  def change
    remove_column :albums, :counter, :integer
    remove_column :albums, :album_counter, :integer
  end
end
