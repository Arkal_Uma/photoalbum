class RemovePhotoCountFromPhotos < ActiveRecord::Migration[5.0]
  def change
    remove_column :photos, :photo_count, :integer
  end
end
