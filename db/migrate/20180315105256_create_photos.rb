class CreatePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :photos do |t|
      t.string :tagling_photo
      t.string :owner
      t.integer :album_id

      t.timestamps
    end
  end
end
