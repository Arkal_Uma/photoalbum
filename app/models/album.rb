class Album < ApplicationRecord
	belongs_to :user
	has_many :photos, :dependent => :destroy
	validates_length_of :photos, maximum: 25,:too_long  => "must have at most {{count}} words"
end
