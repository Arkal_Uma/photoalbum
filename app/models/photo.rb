class Photo < ApplicationRecord
  belongs_to :album
  has_attached_file :image
  validates_attachment :image, presence: true,
  content_type: { content_type: "image/jpeg" },
  size: { in: 0..500.kilobytes }
  validates_associated :album
end

