class User < ApplicationRecord
  has_many :albums
  validates :first_name, length: {maximum: 100, too_long: "%{count} characters is the maximum allowed"},format: {  :with => /\A[a-zA-Z]+\z/,:message => "is not valid"} ,presence: true
  validates :last_name, length: {maximum: 100, too_long: "%{count} characters is the maximum allowed"},format: {  :with => /\A[a-zA-Z]+\z/, :message => "is not valid"}, presence: true
  validates :email, format: {with: /\A[a-zA-Z\+[0-9]\+\-_\.\+a-z\d]+@[a-z\-.]+\.([a-z]{2,})+\z/i}, presence: true
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
end
