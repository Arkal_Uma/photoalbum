class AlbumsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  def new
    @album = Album.new
  end
  def index
  	@albums = Album.all
  end

  def create
    @album = Album.new(album_params)
    @album.user_id = current_user.id
    
    if @album.save
      redirect_to albums_path
    else
      render :new
    end
  end
  def destroy
    @album = Album.find(params[:id])
    if @album.destroy 
      redirect_to albums_path
    else
      render :new
    end
  end
  def show
    @album = Album.find params[:id]
  end
  private
  def album_params
   params.require(:album).permit(:title)
  end
end
